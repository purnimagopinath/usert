class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.date :dob
      t.decimal :salary,precision:10,scale:2
      t.string :location

      t.timestamps
    end
  end
end

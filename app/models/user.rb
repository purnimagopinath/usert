class User < ApplicationRecord
  validates :name,presence:true
  def self.search(salary,location)
    where('lower(location) = ? or salary=?', location.downcase,salary);
  end
end

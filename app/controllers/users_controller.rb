class UsersController < ApplicationController
  http_basic_authenticate_with name:"iam",password:"iam",except:[:index,:show]
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    if params[:salary].present? || params[:location].present?
      @users = User.search(params[:salary].rstrip,params[:location].rstrip)
    else
      @users = User.all
    end
  end
  def new
    @user = User.new
  end
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def edit

  end
  def show

  end
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :dob, :salary, :location)
  end
end
